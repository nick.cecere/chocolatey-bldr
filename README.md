# chocolatey-bldr Visual Studio Code Extension

This extention adds snippets to help build chocolatey packages

## Snippets

### Chocolatey Install file

* `ChocolateyPackage` : Used when modifying the `chocolateyinstall.ps1` file, this will create a snippet for installing an application from a URL.
* `ChocolateyInstallPackage` : Used when modifying the `chocolateyinstall.ps1` file, this will create a snippet for installing an application from a UNC path. 
* `ChocolateyZipPackage` : Used when modifying the `chocolateyinstall.ps1` file, this will create a snippet for installing a zip file from a URL or UNC path.

### Chocolatey Uninstall file

* `ChocolateyUninstall` : Used when modifying the `chocolateyuninstall.ps1` file, this will create a snippet for uninstalling a chocolatey package.

### Chocolatey Nuspec file

* `ChocolateyNuspec` : Used when modifying the `*.nuspec` file, this will create a complete nuspec file.
* `ChocolateyNuspecDependency` : Used when modifying the `*.nuspec` file, this snippet will insert a dependency line into the nuspec file.
* `ChocolateyNuspecDependencyVersion` : Used when modifying the `*.nuspec` file, this snippet will insert a dependency line with version into the nuspec file.

## Dependencies

* Powershell