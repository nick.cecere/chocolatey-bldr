# Change Log

All notable changes to the "chocolatey-bldr" extension will be documented in this file.

## [0.3.0]

- Snippet added to create Uninstall Script
- Snippet added to create nuspec file
- Snippet added to create dependency in nuspec file
- Snippet added to create dependency with version in nuspec file
- Updated Documentation inside Readme file

## [0.2.0]

- Renamed prefixes for ChocolateyInstallPackage, ChocolateyZipPackage, ChocolateyPackage

## [0.1.0]

- Initial pre-release